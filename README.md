# CLI утилита для отслеживания погоды в указанном регионе

Token: e21a403158f379c548b68b5eb37a0cf9

# Технологии

- NodeJS

# Использование

````bash
# npm
npm install

## Using
npm start

# Params
npm start -- params (npm start -- -t Moscow)

- h - Помощь
- t [TOKEN] - Установка токена
- c [CITY] - Установка города
````
