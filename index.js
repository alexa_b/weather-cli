#!/usr/bin/env node

import { parseArgs } from './helpers/parseArgs.js';
import { CLI } from './main/cli.js';

/** @class
 * Точка входа
 */
class Main {
	cli;
	args;

	/** @constructor
	 * @param { object } args 
	*/
	constructor (args) {
		this.args = args;
		this.cli = new CLI(this.args);
	}

	/** @function
	 * Инициализация
	*/
	init() {
		if (this.args.get(this.cli.ARGS_DICTIONARY.help)) {
			this.cli.logService.printHelp();
			return;
		}

		if (this.args.get(this.cli.ARGS_DICTIONARY.city)) {
			this.cli.saveCity();
			return;
		}

		if (this.args.get(this.cli.ARGS_DICTIONARY.token)) {
			this.cli.saveToken();
			return;
		}

		this.cli.getForcast();
	}
}

const main = new Main(parseArgs(process.argv));
main.init();