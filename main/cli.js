import { LogService } from '../services/log.service.js';
import { StorageService } from '../services/storage.service.js';
import { ApiService } from '../services/api.service.js';
import dedent from 'dedent-js';
import chalk from 'chalk';

/** @class
 * Класс приложения
 */
class CLI {
	logService;
	storageService;
	apiService;
	args;

	ARGS_DICTIONARY;

	/** @constructor
 	 * @param { object } args
	 */
	constructor(args) {
		this.logService = new LogService();
		this.storageService = new StorageService();
		this.apiService = new ApiService();
		
		this.args = args;

		this.ARGS_DICTIONARY = {
			help: 'h',
			city: 'c',
			token: 't'
		}
	}

	/** @function
	 * Сохранение токена
	 */
 	async	saveToken() {
		const token = this.args.get(this.ARGS_DICTIONARY.token);
		if (!token?.length) {
			this.logService.printError('Не передан токен');
			return;
		}

		try {
			await this.storageService.saveKeyValue(this.storageService.KEYS_DICTIONARY.token, token);
			this.logService.printSuccess('Токен сохранен');
		} catch (error) {
			this.logService.printError(e.message);
		}
	}

	/** @function
	 * Сохранение города
	 */
 	async	saveCity() {
		const city = this.args.get(this.ARGS_DICTIONARY.city);
		if (!city?.length) {
			this.logService.printError('Не передан город');
			return;
		}

		try {
			await this.storageService.saveKeyValue(this.storageService.KEYS_DICTIONARY.city, city);
			this.logService.printSuccess('Город сохранен');
		} catch (error) {
			this.logService.printError(e.message);
		}
	}

	/** @function
	 * Получение погоды
	 */
	async getForcast() {
		try {
			const city = await this.storageService.getKeyValue(this.storageService.KEYS_DICTIONARY.city);
			if (!city) {
				throw new Error(`Не задан город, задайте его через команду ${this.storageService.KEYS_DICTIONARY.city} [CITY]`);
			}

			const data = await this.apiService.getWeather(city);
			this.printWeather(data, this.apiService.getIcon(data.weather[0].icon));
		} catch (error) {
			if (error?.response?.status === 401) {
				this.logService.printError('Неверно указан токен');
				return;
			}

			if (error?.response?.status === 404) {
				this.logService.printError('Неверно указан город');
				return;
			}

			this.logService.printError(error.message ?? 'Произошла непредвиденная ошибка');
		}
	}

	/** @function
	 * Вывод погоды
	 * @param {object} response
	 * @param {string} icon 
	 */
	printWeather(response, icon) {
		const { name, weather, main, wind } = response;
		const [ { description: weatherDescription } ] = weather;
		const { temp, feels_like: feelsLike, humidity } = main;
		const { speed } = wind;

		console.log(
			dedent`${chalk.bgYellow(' WEATHER ')} Погода в городе ${name}
			${icon}  ${weatherDescription}
			Температура: ${temp} (ощущается как ${feelsLike})
			Влажность: ${humidity}%
			Скорость ветра: ${speed}%
			`
		);
	}
}

export {
	CLI
}