/** @function
 * Парсинг аргуметов из консоли
 * @param { object } args 
 * @returns { object }
 */
const parseArgs = (args) => {
	const result = new Map();

	const [ _executer, _file, ...rest ] = args;
	rest.forEach((value, index, array) => {
		if (!value.startsWith('-')) {
			return
		}

		const nextValue = array[index + 1];
		const resValue = value.substring(1);

		if (!nextValue || nextValue.startsWith('-')) {
			result.set(resValue, true);
		} else {
			result.set(resValue, nextValue);
		}
	});

	return result;
};

export {
	parseArgs
};