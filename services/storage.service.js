import { homedir } from 'os';
import { join } from 'path';
import { promises } from 'fs';

/** @class
 * Работа с хранилищем
 */
class StorageService {
	filePath;
	KEYS_DICTIONARY;

	/** @constructor */
	constructor() {
		this.filePath = join(homedir(), 'weather-data.json');

		this.KEYS_DICTIONARY = {
			token: 'token',
			city: 'city'
		}
	}

	/** @function
	 * Создан ли уже файл
	 */
	async isExist() {
		try {
			await promises.stat(this.filePath);
			return true;
		} catch (error) {
			return false;
		}
	}

	/** @function
	 * Получение значения по ключу
	 * @param { string } key 
	 */
	async getKeyValue (key) {
		if (await this.isExist()) {
			const file = await promises.readFile(this.filePath);
			const data = JSON.parse(file);
			return data[key];
		};

		return;
	}

	/** @function
	 * Установка значения по ключу
	 * @param { string } key 
	 * @param { string } value 
	 */
	async saveKeyValue(key, value) {
		let data = {};

		if (await this.isExist()) {
			const file = await promises.readFile(this.filePath);
			data = JSON.parse(file);
		};

		data[key] = value;

		await promises.writeFile(this.filePath, JSON.stringify(data));
	}
}

export {
	StorageService
}