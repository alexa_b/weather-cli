import axios from 'axios';
import { StorageService } from './storage.service.js';

/** @class
 * Взаимодействие с API
 */
class ApiService {
	storageService;

	/** @constructor */
	constructor() {
		this.storageService = new StorageService();
	}

	/** @function
	 * Получение координат
	 * @param {string} string - Название города
	 */
	async getWeather(cityName) {
		const token = await this.storageService.getKeyValue(this.storageService.KEYS_DICTIONARY.token);
		if (!token) {
			throw new Error(`Не задан ключ API, задайте его через команду ${this.storageService.KEYS_DICTIONARY.token} [API_KEY]`);
		}

		const { data } = await axios.get('https://api.openweathermap.org/data/2.5/weather', {
			params: {
				q: cityName,
				appid: token,
				lang: 'ru',
				units: 'metric'
			}
		});

		return data;
	}

	/** @function
	 * Получение координат
	 * @param {string} icon - Иконка из response 
	 */
	getIcon(icon) {
		const value = icon.slice(0, -1);

		switch (value) {
			case '01':
				return '☀️';
			case '02':
				return '🌤️';
			case '03':
				return '☁️';
			case '04':
				return '☁️';
			case '09':
				return '🌧️';
			case '10':
				return '🌦️';
			case '11':
				return '🌩️';
			case '13':
				return '❄️';
			case '50':
				return '🌫️';
			default:
				return '';
		}
	};
}

export {
	ApiService
}