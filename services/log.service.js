import chalk from 'chalk';
import dedent from 'dedent-js';

/**
 * @class
 * Логгер
 */
class LogService {
	/**
	 * @function
	 * Вывод ошибки
	 * @param { string } error 
	 */
	printError(error) {
		console.log(chalk.bgRed(' Error ') + ' ' + error);
	}

	/**
	 * @function
	 * Вывод успеха
	 * @param { string } message 
	 */
	printSuccess(message) {
		console.log(chalk.bgGreen(' Успех ') + ' ' + message);
	}

	/**
	 * @function
	 * Вывод помощи
	 */
	printHelp() {
		console.log(
			dedent`${chalk.bgCyan(' HELP ')}
			Без параметров - вывод погоды
			-s [CITY] для установки города
			-h для вывода помощи
			-t [API_KEY] для сохранения токена
			`
		);
	}
}

export {
	LogService
}